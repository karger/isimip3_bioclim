import numpy as np
import xarray as xr
import pandas as pd
import gcsfs
import datetime
import fsspec
from dask.diagnostics import ProgressBar
from chelsa_cmip6.BioClim import BioClim
from pyesgf.search import SearchConnection



def _get_cmip(activity_id, table_id, variable_id, experiment_id, instituion_id, source_id, member_id):
    """
    Get CMIP model from Google Cloud storage via lazy loading.

    :param activity_id: the activity_id according to CMIP6
    :param table_id: the table id according to CMIP6
    :param experiment_id: the experiment_id according to CMIP6
    :param instituion_id: the instituion_id according to CMIP6
    :param source_id: the source_id according to CMIP6
    :param member_id: the member_id according to CMIP6

    :return: xarray dataset
    :rtype: xarray
    """
    gcs = gcsfs.GCSFileSystem(token='anon')
    df = pd.read_csv('https://storage.googleapis.com/cmip6/cmip6-zarr-consolidated-stores.csv')
    search_string = "activity_id == '" + activity_id + "' & table_id == '" + table_id + "' & variable_id == '" + variable_id + "' & experiment_id == '" + experiment_id + "' & institution_id == '" + instituion_id + "' & source_id == '" + source_id + "' & member_id == '" + member_id + "'"
    df_ta = df.query(search_string)
    # get the path to a specific zarr store (the first one from the dataframe above)
    zstore = df_ta.zstore.values[-1]
    # create a mutable-mapping-style interface to the store
    mapper = gcs.get_mapper(zstore)
    # open it using xarray and zarr
    ds = xr.open_zarr(mapper, consolidated=True)
    try:
        ds['time'] = np.sort(ds['time'].values)
    except Exception:
        pass

    return ds


class chelsaV2:
    """
    Class to download and clip data from the CHELSA V2.1 normals (climatologies)
    for a specific bounding box delimited by minimum and maximum latitude and longitude

    :param xmin: Minimum longitude [Decimal degree]
    :param xmax: Maximum longitude [Decimal degree]
    :param ymin: Minimum latitude [Decimal degree]
    :param ymax: Maximum latitude [Decimal degree]
    :param variable_id: id of the variable that needs to be downloaded (e.g. 'tas')

    """

    def __init__(self, xmin, xmax, ymin, ymax, variable_id):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.variable_id = variable_id

    def _crop_ds_(self, ds):
        """
        clip xarray

        :param ds: a xarray to_dataset
        :return: clipped xarray
        :rtype: xarray
        """
        mask_lon = (ds.lon >= self.xmin) & (ds.lon <= self.xmax)
        mask_lat = (ds.lat >= self.ymin) & (ds.lat <= self.ymax)
        cropped_ds = ds.where(mask_lon & mask_lat, drop=True)
        return cropped_ds

    def get_chelsa(self):
        """
        download chelsa

        :return: cropped xarray
        :rtype: xarray
        """

        a = []
        for month in range(1, 13):
            url = 'https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V2/GLOBAL/climatologies/1981-2010/ncdf/CHELSA_' + self.variable_id + '_' + '%02d' % (
                month,) + '_1981-2010_V.2.1.nc'
            with fsspec.open(url) as fobj:
                ds = xr.open_dataset(fobj).chunk({'lat': 500, 'lon': 500})
                ds = self._crop_ds_(ds)
                ds.load()
            a.append(ds)

        ds = xr.concat([i for i in a], 'time')

        # ds = self._crop_ds_(xr.concat([i for i in a], 'time'))

        # old version using rasterio
        # a = []
        # for month in range(1, 13):
        #    url = 'https://envicloud.os.zhdk.cloud.switch.ch/chelsa/chelsa_V2/GLOBAL/climatologies/1981-2010/' + self.variable_id + '/CHELSA_' + self.variable_id + '_' + '%02d' % (month,) + '_1981-2010_V.2.1.tif'
        #    a.append(url)

        # ds = self._crop_ds_(xr.concat([xr.open_rasterio(i) for i in a], 'time'))
        if self.variable_id == "tas" or self.variable_id == 'tasmin' or self.variable_id == 'tasmax':
            res = ds.assign(Band1=ds['Band1'] * 0.1)
        if self.variable_id == 'pr':
            res = ds.assign(Band1=ds['Band1'] * 0.1)

        return res


def extract_time_series(ds, coords):
    """
    Extracts time series values from an xarray dataset at specific coordinates provided in a DataFrame.

    Parameters:
    ds (xarray.Dataset): The xarray dataset from which to extract values.
    coords (pandas.DataFrame): A DataFrame containing coordinates with columns: id, latitude, and longitude.

    Returns:
    pandas.DataFrame: A DataFrame containing the extracted time series values with the corresponding ids.
    """
    # Ensure latitude and longitude are read as floats
    coords['latitude'] = coords['latitude'].astype(float)
    coords['longitude'] = coords['longitude'].astype(float)

    # Determine the correct coordinate names
    if 'latitude' in ds.coords:
        lat_name = 'latitude'
    elif 'lat' in ds.coords:
        lat_name = 'lat'
    else:
        raise ValueError("Dataset does not contain 'latitude' or 'lat' coordinate.")

    if 'longitude' in ds.coords:
        lon_name = 'longitude'
    elif 'lon' in ds.coords:
        lon_name = 'lon'
    else:
        raise ValueError("Dataset does not contain 'longitude' or 'lon' coordinate.")

    # Extract time series values from the xarray dataset at the specified coordinates
    time_series_data = []

    for _, row in coords.iterrows():
        point_id = row['id']
        lat = row['latitude']
        lon = row['longitude']

        # Use the xarray's sel method to get the value at the specific coordinates
        time_series = ds.sel({lat_name: lat, lon_name: lon}, method='nearest')

        # Convert to DataFrame and add 'id' column
        time_series_df = time_series.to_dataframe().reset_index()
        time_series_df['id'] = point_id

        # Append to the list
        time_series_data.append(time_series_df)

    # Combine all time series data into a single DataFrame
    out = pd.concat(time_series_data)

    return out


ch_tas = chelsaV2(xmin=6,
                  xmax=11,
                  ymin=46,
                  ymax=48,
                  variable_id="tas").get_chelsa()


cmip = _get_cmip(activity_id="ScenarioMIP",
                 table_id="Amon",
                 variable_id="tas",
                 experiment_id="ssp585",
                 instituion_id="MPI-M",
                 source_id="MPI-ESM1-2-LR",
                 member_id="r1i1p1f1")

hist = _get_cmip(activity_id="CMIP",
                 table_id="Amon",
                 variable_id="tas",
                 experiment_id="historical",
                 instituion_id="MPI-M",
                 source_id="MPI-ESM1-2-LR",
                 member_id="r1i1p1f1")

ds_combined = xr.concat([hist, cmip], dim='time')

import pandas as pd

# Load the coordinates from the text file
coords = pd.read_csv('/home/karger/scratch/Coordinates_selected_forest_reserves.txt', delim_whitespace=True)

coords['latitude'] = coords['latitude'].astype(float)
coords['longitude'] = coords['longitude'].astype(float)

chelsa_tas = extract_time_series(ch_tas, coords)
cmip_tas = extract_time_series(ds_combined,coords)

ds_selected_years = ds_combined.sel(time=slice('1981-01-01', '2010-12-31'))

# Step 2: Calculate the monthly climatologies
monthly_climatologies = ds_selected_years.groupby('time.month').mean('time')

cmip_clim = extract_time_series(monthly_climatologies, coords)


# Step 1: Merge the datasets based on month and id
merged_df = pd.merge(chelsa_tas, df_model, on=['Month', 'id'], suffixes=('_ref', '_model'))
merged_df = pd.merge(merged_df, df_long_model, on=['Month', 'id'], suffixes=('_model', '_long'))

# Step 2: Calculate bias correction
# Calculate the bias correction as the difference between the reference and model run, or as the ratio
bias_correction = merged_df['Value_ref'] - merged_df['Value_model']
# Or bias_correction = merged_df['Value_ref'] / merged_df['Value_model']

# Step 3: Apply bias correction to the long model run dataset
# Add the bias correction to the long model run dataset
df_long_model_corrected = merged_df['Value_long'] + bias_correction
# Step 3: Extract time series values from the xarray dataset at the specified coordinates
time_series_data = []

for _, row in coords.iterrows():
    point_id = row['id']
    lat = row['latitude']
    lon = row['longitude']

    # Use the xarray's sel method to get the value at the specific coordinates
    time_series = ch_tas.sel(lat=lat, lon=lon, method='nearest')

    # Convert to DataFrame and add 'id' column
    time_series_df = time_series.to_dataframe().reset_index()
    time_series_df['id'] = point_id

    # Append to the list
    time_series_data.append(time_series_df)

# Combine all time series data into a single DataFram
#
out = pd.concat(time_series_data)

# Save the results to a new file if needed
all_time_series_df.to_csv('extracted_time_series.csv', index=False)

# Save the results to a new file if needed
values_df.to_csv('/home/karger/scratch/extracted_values.csv', index=False)
