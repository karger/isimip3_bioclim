import pandas as pd
import xarray as xr
import os
from glob import glob
from tqdm import tqdm
from multiprocessing import Pool, cpu_count
import shutil

def extract_single_coordinate(args):
    """
    Function to extract data for a single coordinate from NetCDF files.
    """
    ds, variable, location_id, lon, lat = args

    try:
        # Select the nearest data point using 'lat' and 'lon'
        selected_data = ds.sel(lon=lon, lat=lat, method='nearest')[variable].to_dataframe().reset_index()

        # Add the location information to the DataFrame
        selected_data['id'] = location_id
        selected_data['longitude'] = lon
        selected_data['latitude'] = lat

        return selected_data

    except Exception as e:
        print(f"Error processing coordinate {location_id}: {str(e)}")
        return pd.DataFrame()

def copy_nc_files_to_temp(nc_files, temp_dir):
    """
    Copy NetCDF files to a temporary directory if they are not already there.
    """
    os.makedirs(temp_dir, exist_ok=True)
    temp_nc_files = []

    for nc_file in tqdm(nc_files, desc='Copying NetCDF files'):
        temp_nc_file = os.path.join(temp_dir, os.path.basename(nc_file))

        if not os.path.exists(temp_nc_file):
            shutil.copy(nc_file, temp_nc_file)

        temp_nc_files.append(temp_nc_file)

    return temp_nc_files

def extract_pixel_values(nc_directory, csv_file, model, member, scenario, variable, output_csv, temp_dir='/home/karger/scratch/', n_workers=None):
    temp_nc_files = []  # Ensure temp_nc_files is defined
    try:
        # Read coordinates from CSV file
        coords_df = pd.read_csv(csv_file, sep=';')
        coords_df.set_index('id', inplace=True)

        # Convert the coordinates DataFrame to a Dataset
        crd_ix = coords_df.to_xarray()

        # Construct file pattern for NetCDF files
        file_pattern = f"{model}_{member}_w5e5_{scenario}_{variable}_global_daily_*.nc"
        nc_files = glob(os.path.join(nc_directory, file_pattern))

        if not nc_files:
            raise FileNotFoundError(f"No NetCDF files found for pattern: {file_pattern}")

        # Copy NetCDF files to temporary directory if they are not already there
        temp_nc_files = copy_nc_files_to_temp(nc_files, temp_dir)

        # Open the NetCDF files as a single dataset
        ds = xr.open_mfdataset(temp_nc_files, combine='by_coords').load()

        # Prepare arguments for multiprocessing
        args = [(ds, variable, location_id, lon, lat) for location_id, lon, lat in zip(coords_df.index, coords_df['longitude'], coords_df['latitude'])]

        # Determine number of workers if not specified
        if n_workers is None:
            n_workers = cpu_count()

        # Use multiprocessing Pool to parallelize the extraction process with a progress bar
        with Pool(processes=n_workers) as pool:
            all_results = []
            for result in tqdm(pool.imap(extract_single_coordinate, args), total=len(args)):
                all_results.append(result)

        # Concatenate all results into a single DataFrame
        results_df = pd.concat([result for result in all_results if not result.empty], ignore_index=True)

        # Save the results to a CSV file
        results_df.to_csv(output_csv, index=False)

        print(f"Data successfully extracted and saved to {output_csv}")

    except Exception as e:
        print(f"Error during extraction: {str(e)}")

    finally:
        # Close the dataset
        if 'ds' in locals():
            ds.close()

        # Clean up: Remove temporary NetCDF files
        for temp_nc_file in temp_nc_files:
            if os.path.exists(temp_nc_file):
                os.remove(temp_nc_file)

csv_file = '/home/karger/scratch/All_combined_locality_land_june_21_NID.csv'
extract_pixel_values(nc_directory='/storage/zilker/ISIMIP3bClimateData', csv_file=csv_file,
                      model='gfdl-esm4', member='r1i1p1f1', scenario='historical',
                      variable='tas', output_csv='/home/karger/output_test.csv',
                      temp_dir='/home/karger/scratch/')




# try it with saga
#! /usr/bin/env python

#_________________________________________
##########################################

# Initialize the environment...

# Windows: Let the 'SAGA_PATH' environment variable point to
# the SAGA installation folder before importing 'saga_api'!
# This can be defined globally in the Windows system or
# user environment variable settings, in the 'PySAGA/__init__.py'
# file, or in the individual Python script itself. To do the latter
# just uncomment the following line and adjust the path accordingly:
###import os; os.environ['SAGA_PATH'] = 'C:/Program Files/SAGA/'

# Windows: The most convenient way to make PySAGA available to all your
# Python scripts is to copy the PySAGA folder to the 'Lib/site-packages/'
# folder of your Python installation. If don't want to do this or if you
# don't have the rights to do so, you can also copy it to the folder with
# the Python scripts in which you want to use PySAGA, or alternatively
# you can add the path containing the PySAGA folder (e.g. the path to your
# SAGA installation) to the PYTHONPATH environment variable. To do this
# from within your script you can also take the following command (just
# uncomment the following line and adjust the path accordingly):
###import sys; sys.path.insert(1, 'C:/Program Files/SAGA/')


#_________________________________________
##########################################

import saga_api
import os
import sys


def Load_Tool_Libraries(Verbose):
    saga_api.SG_UI_Msg_Lock(True)        # Linux
    saga_api.SG_Get_Tool_Library_Manager().Add_Directory('/usr/local/lib/saga', False)
    # Or set the Tool directory like this!
    saga_api.SG_UI_Msg_Lock(False)
    if Verbose == True:
                print('Python - Version ' + sys.version)
                print(saga_api.SAGA_API_Get_Version())
                print('number of loaded libraries: ' + str(saga_api.SG_Get_Tool_Library_Manager().Get_Count()))
                print()
    return saga_api.SG_Get_Tool_Library_Manager().Get_Count()

def load_sagadata(path_to_sagadata):

    saga_api.SG_Set_History_Depth(0)    # History will not be created
    saga_api_dataobject = 0             # initial value

    # CSG_Grid -> Grid
    if any(s in path_to_sagadata for s in (".sgrd", ".sg-grd", "sg-grd-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Grid(str(path_to_sagadata))

    # CSG_Grids -> Grid Collection
    if any(s in path_to_sagadata for s in ("sg-gds", "sg-gds-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Grids(str(path_to_sagadata))

    # CSG_Table -> Table
    if any(s in path_to_sagadata for s in (".txt", ".csv", ".dbf")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Table(str(path_to_sagadata))

    # CSG_Shapes -> Shapefile
    if '.shp' in path_to_sagadata:
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Shapes(str(path_to_sagadata))

    # CSG_PointCloud -> Point Cloud
    if any(s in path_to_sagadata for s in (".spc", ".sg-pts", ".sg-pts-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_PointCloud(str(path_to_sagadata))

    if saga_api_dataobject == None or saga_api_dataobject.is_Valid() == 0:
        print('ERROR: loading [' + path_to_sagadata + ']')
        return 0

    print('File: [' + path_to_sagadata + '] has been loaded')
    return saga_api_dataobject


def Run_Add_Grid_Values_to_Points(shapes, grids, n):
    # Get the tool:
    Tool = saga_api.SG_Get_Tool_Library_Manager().Get_Tool('shapes_grid', '0')
    if not Tool:
        print('Failed to request tool: shapes_grid')
        return False

    # Set the parameter interface:
    Tool.Reset()
    Tool.Set_Parameter('SHAPES', shapes)
    for i in range(0,n-1):
        Tool.Get_Parameter('GRIDS').asList().Add_Item(grids.asGridList().Get_Grid(i))

    Tool.Set_Parameter('RESULT', saga_api.SG_Get_Create_Pointer()) # optional output, remove this line, if you don't want to create it
    Tool.Set_Parameter('RESAMPLING', 0) # 'B-Spline Interpolation'

    # Execute the tool:
    if not Tool.Execute():
        print('failed to execute tool: ' + Tool.Get_Name().c_str())
        return False

    # Request the results:
    Data = Tool.Get_Parameter('RESULT').asDataObject()

    return Data


def import_ncdf(ncdffile):
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '6')
    if not Tool:
        print('Failed to request tool: Import NetCDF')
        return False

    Tool.Reset()
    Tool.Set_Parameter('FILE', ncdffile)
    Tool.Set_Parameter('SAVE_FILE', False)
    Tool.Set_Parameter('SAVE_PATH', '')
    Tool.Set_Parameter('TRANSFORM', True)
    Tool.Set_Parameter('RESAMPLING', 0)
    if not Tool.Execute():
        print('failed to execute tool: ' + Tool.Get_Name().c_str())
        return False
    Data = Tool.Get_Parameter('GRIDS').asGridList()
    return Data


def export_xyz(shape, filename):
    Tool = saga_api.SG_Get_Tool_Library_Manager().Get_Tool('io_shapes', '2')
    if not Tool:
        print('Failed to request tool: Export Shapes to XYZ')
        return False

    Tool.Reset()
    Tool.Set_Parameter('POINTS', shape)
    Tool.Set_Parameter('FIELD', '<no attributes>')
    Tool.Set_Parameter('HEADER', True)
    Tool.Set_Parameter('SEPARATE', 0) # 'none'
    Tool.Set_Parameter('FILENAME', filename)

    if not Tool.Execute():
        print('failed to execute tool: ' + Tool.Get_Name().c_str())
        return False



Load_Tool_Libraries(True)


models = ['gfdl-esm4', 'ipsl-cm6a-lr', 'mpi-esm1-2-hr', 'mri-esm2-0']
ssps =  ['ssp126', 'ssp370', 'ssp585']
for scenario in ['historical']:
    decades = [f"{start}_{start + 9}" for start in range(1851, 2011, 10)]
    decades.insert(0, '1850_1850')
    decades.append('2011_2014')
    for years in decades:
        for variable in ['tas', 'pr']:
            for model in models:
                member = 'r1i1p1f1'
                if model == 'ukesm1-0-ll':
                    member = 'r1i1p1f2'

                ncds = import_ncdf('/storage/zilker/ISIMIP3bClimateData/' + model + '_' + member + '_w5e5_' + scenario + '_' + variable + '_global_daily_' + years + '.nc')
                n = ncds.Get_Grid_Count()

                shp1 = load_sagadata('/home/karger/scratch/All_combined_locality_land_june_21_NID2.shp')

                out1 = Run_Add_Grid_Values_to_Points(shp1, ncds, n)
                fileout = '/storage/karger/scratch/' + model + '_' + member + '_w5e5_' + scenario + '_' + variable + '_global_daily_' + years + '.txt'
                export_xyz(out1, fileout)

                saga_api.SG_Get_Data_Manager().Delete_All()


for scenario in ssps:
    decades = [f"{start}_{start + 9}" for start in range(2021, 2101, 10)]
    decades.insert(0, '2015_2020')
    for years in decades:
        for variable in ['tas', 'pr']:
            for model in models:
                member = 'r1i1p1f1'
                if model == 'ukesm1-0-ll':
                    member = 'r1i1p1f2'

                ncds = import_ncdf('/storage/zilker/ISIMIP3bClimateData/' + model + '_' + member + '_w5e5_' + scenario + '_' + variable + '_global_daily_' + years + '.nc')
                n = ncds.Get_Grid_Count()

                shp1 = load_sagadata('/home/karger/scratch/All_combined_locality_land_june_21_NID2.shp')

                out1 = Run_Add_Grid_Values_to_Points(shp1, ncds, n)
                fileout = '/storage/karger/scratch/' + model + '_' + member + '_w5e5_' + scenario + '_' + variable + '_global_daily_' + years + '.txt'
                export_xyz(out1, fileout)

                saga_api.SG_Get_Data_Manager().Delete_All()




models = ['gfdl-esm4', 'ipsl-cm6a-lr', 'mpi-esm1-2-hr', 'mri-esm2-0', 'ukesm1-0-ll']

coords = '/home/karger/scratch/All_combined_locality_land_june_21_NID.csv'

for scenario in ['historical', 'ssp126', 'ssp370', 'ssp585']:
    for variable in ['tas', 'pr']:
        for model in models:
            member = 'r1i1p1f1'
            if model == 'ukesm1-0-ll':
                member = 'r1i1p1f2'

             extract_pixel_values(
                nc_directory='/storage/zilker/ISIMIP3bClimateData',
                csv_file=coords,
                model=model,
                member=member,
                scenario=scenario,
                variable=variable,
                output_csv='/home/karger/' + model + '_' + member + '_' + scenario + '_' + variable + '.csv'
         )




import numpy as np
import xarray as xr
import pandas as pd
import gcsfs
import datetime
import fsspec
from dask.diagnostics import ProgressBar
from chelsa_cmip6.BioClim import BioClim
from pyesgf.search import SearchConnection










def _get_cmip(activity_id, table_id, variable_id, experiment_id, institution_id, source_id, member_id):
    gcs = gcsfs.GCSFileSystem(token='anon')
    df = pd.read_csv('https://storage.googleapis.com/cmip6/cmip6-zarr-consolidated-stores.csv')
    search_string = (
        f"activity_id == '{activity_id}' & table_id == '{table_id}' & variable_id == '{variable_id}' & "
        f"experiment_id == '{experiment_id}' & institution_id == '{institution_id}' & source_id == '{source_id}' & "
        f"member_id == '{member_id}'"
    )
    df_ta = df.query(search_string)
    zstore = df_ta.zstore.values[-1]
    mapper = gcs.get_mapper(zstore)
    ds = xr.open_zarr(mapper, consolidated=True)
    try:
        ds['time'] = np.sort(ds['time'].values)
    except Exception:
        pass
    return ds


def extract_single_coordinate(args):
    ds, variable_id, row = args
    lat = row['latitude']
    lon = row['longitude']
    location_id = row['id']

    selected_data = ds.sel(lat=lat, lon=lon, method='nearest')[variable_id].load().to_dataframe().reset_index()

    selected_data['id'] = location_id
    selected_data['latitude'] = lat
    selected_data['longitude'] = lon

    return selected_data


def compute_climatology(ds, start_year, end_year):
    climatology_ds = ds.sel(time=slice(f'{start_year}-01-01', f'{end_year}-12-31'))
    climatology_ds = climatology_ds.groupby('time.month').mean(dim='time')
    return climatology_ds


def extract_pixel_values(activity_id, table_id, variable_id, experiment_id, institution_id, source_id, member_id, csv_file, output_csv=None, n_workers=4):
    coords_df = pd.read_csv(csv_file, sep=';')

    ds = _get_cmip(activity_id, table_id, variable_id, experiment_id, institution_id, source_id, member_id)
    ds = ds.resample(time='M').mean()

    with ProgressBar():
        ds.load()

    args = [(ds, variable_id, row) for idx, row in coords_df.iterrows()]

    with Pool(processes=n_workers) as pool:
        all_results = pool.map(extract_single_coordinate, args)

    results_df = pd.concat(all_results, ignore_index=True)

    if output_csv:
        results_df.to_csv(output_csv, index=False)
        print(f"Data successfully extracted and saved to {output_csv}")

    if experiment_id == 'historical':
        climatology_ds = compute_climatology(ds, 1981, 2010)
        all_climatology_results = []

        for idx, row in coords_df.iterrows():
            lat = row['latitude']
            lon = row['longitude']
            location_id = row['id']

            selected_data = climatology_ds.sel(lat=lat, lon=lon, method='nearest')[variable_id].load().to_dataframe().reset_index()

            selected_data['id'] = location_id
            selected_data['latitude'] = lat
            selected_data['longitude'] = lon

            all_climatology_results.append(selected_data)

        climatology_results_df = pd.concat(all_climatology_results, ignore_index=True)
        return results_df, climatology_results_df

    return results_df



def extract_chelsa_single_coordinate(args):
    url, row, month, variable_id = args
    lat = row['latitude']
    lon = row['longitude']
    location_id = row['id']

    with fsspec.open(url) as fobj:
        ds = xr.open_dataset(fobj)
        selected_data = ds.sel(lat=lat, lon=lon, method='nearest')['Band1'].load()

        # Rename and adjust the variable
        if selected_data.ndim == 0:  # Single scalar value
            selected_data = pd.DataFrame({'month': [month], variable_id: [selected_data.values]})
        else:  # Convert to DataFrame
            selected_data = selected_data.to_dataframe().reset_index()
            selected_data = selected_data.rename(columns={'Band1': variable_id})
            selected_data['month'] = month

        # Adjust variable values
        if variable_id in ["tas", "tasmin", "tasmax", "pr"]:
            selected_data[variable_id] *= 0.1

        if variable_id == "pr":
            # Convert from kg/m²/s to mm/month
            seconds_in_month = 2592000
            selected_data[variable_id] = selected_data[variable_id] / seconds_in_month

    selected_data['id'] = location_id
    selected_data['latitude'] = lat
    selected_data['longitude'] = lon

    return selected_data


def extract_chelsa(variable_id, csv_file, n_workers=4):
    coords_df = pd.read_csv(csv_file, sep=';')
    urls = [f'https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V2/GLOBAL/climatologies/1981-2010/ncdf/CHELSA_{variable_id}_{month:02d}_1981-2010_V.2.1.nc' for month in range(1, 13)]

    args = [(url, row, month, variable_id) for month, url in enumerate(urls, start=1) for _, row in coords_df.iterrows()]

    with Pool(processes=n_workers) as pool:
        # Use tqdm to create a progress bar
        all_chelsa_results = list(tqdm(pool.imap(extract_chelsa_single_coordinate, args), total=len(args), desc="Extracting CHELSA data"))

    chelsa_results_df = pd.concat(all_chelsa_results, ignore_index=True)

    return chelsa_results_df


def calculate_differences(clim_df, chelsa_df, variable_id, method='difference', constant=1e-6):
    """
    Calculate the difference or ratio between two DataFrames for a given variable per location ID and month.

    :param clim_df: DataFrame containing the climatology data
    :param chelsa_df: DataFrame containing the CHELSA data
    :param variable_id: The variable to calculate the difference for
    :param method: Method to calculate difference ('difference' or 'ratio')
    :param constant: Small constant to avoid division by zero in ratio calculation

    :return: DataFrame containing the location ID, month, and the calculated difference or ratio
    """
    # Ensure the DataFrames contain the required columns
    if variable_id not in clim_df.columns or variable_id not in chelsa_df.columns:
        raise ValueError(f"The variable_id '{variable_id}' must be present in both DataFrames.")

    # Merge the DataFrames on 'id' and 'month'
    merged_df = pd.merge(clim_df, chelsa_df, on=['id', 'month'], suffixes=('_clim', '_chelsa'))

    # Calculate the difference or ratio
    if method == 'difference':
        merged_df['difference'] = merged_df[f'{variable_id}_chelsa'] - merged_df[f'{variable_id}_clim']
    elif method == 'ratio':
        merged_df['difference'] = (merged_df[f'{variable_id}_chelsa'] + constant) / (merged_df[f'{variable_id}_clim'] + constant)
    else:
        raise ValueError("Method must be either 'difference' or 'ratio'.")

    # Select relevant columns for the output
    result_df = merged_df[['id', 'month', 'difference']]

    return result_df


def convert_time_to_month(df):
    """
    Convert the 'time' column in DataFrame to months represented as integers (1-12),
    while keeping the original 'time' column.

    :param df: DataFrame containing the 'time' column as datetime objects.

    :return: DataFrame with 'time' and 'month' columns.
    """
    df['month'] = df['time'].dt.month
    return df


def apply_differences(hist_df, cmip_df, difference_df, variable_id, method='add', constant=1e-6):
    """
    Apply differences or ratios from difference_df to the concatenated hist and cmip DataFrames.

    :param hist_df: DataFrame containing historical data
    :param cmip_df: DataFrame containing cmip data
    :param difference_df: DataFrame containing the calculated differences or ratios
    :param variable_id: The variable to apply the differences to
    :param method: Method to apply difference ('add' or 'multiply')
    :param constant: Small constant to avoid division by zero in ratio calculation

    :return: DataFrame with the differences or ratios applied, retaining 'time' column
    """
    # Convert 'time' to 'month' in hist and cmip DataFrames
    hist_df = convert_time_to_month(hist_df)
    cmip_df = convert_time_to_month(cmip_df)

    # Ensure the DataFrames contain the required columns
    if variable_id not in hist_df.columns or variable_id not in cmip_df.columns or 'difference' not in difference_df.columns:
        raise ValueError(f"The variable_id '{variable_id}' must be present in hist_df and cmip_df, and 'difference' in difference_df.")

    # Merge the combined DataFrame with the difference DataFrame on 'id' and 'month'
    merged_df = pd.merge(pd.concat([hist_df, cmip_df], ignore_index=True), difference_df, on=['id', 'month'], suffixes=('', '_diff'))

    # Apply the difference or ratio
    if method == 'add':
        merged_df[variable_id] = merged_df[variable_id] + merged_df['difference']
    elif method == 'multiply':
        merged_df[variable_id] = (merged_df[variable_id] + constant) * (merged_df['difference'] + constant)
    else:
        raise ValueError("Method must be either 'add' or 'multiply'.")

    # Drop the 'difference' column after applying it
    merged_df = merged_df.drop(columns=['difference', 'month'])

    return merged_df



def delta_change_coords(coords, variable_id, table_id, experiment_id, institution_id, source_id, member_id, output_csv_hist, output_csv_cmip, outdir, n_workers=10, method="add"):
    # Extract CHELSA data
    chelsa_results_df = extract_chelsa(variable_id=variable_id, csv_file=coords, n_workers=n_workers)
    chelsa_results_df.to_csv(outdir + '/chelsa_df.csv', index=False)

    # Extract historical and scenario data
    hist, clim = extract_pixel_values(
        activity_id="CMIP",
        table_id=table_id,
        variable_id=variable_id,
        experiment_id="historical",
        institution_id=institution_id,
        source_id=source_id,
        member_id=member_id,
        csv_file=coords,
        output_csv=output_csv_hist,
        n_workers=n_workers
    )

    cmip = extract_pixel_values(
        activity_id='ScenarioMIP',
        table_id=table_id,
        variable_id=variable_id,
        experiment_id=experiment_id,
        institution_id=institution_id,
        source_id=source_id,
        member_id=member_id,
        csv_file=coords,
        output_csv=output_csv_cmip,
        n_workers=n_workers
    )

    if method == "add":
        mm = 'difference'

    if method == "multiply":
        mm = 'ratio'

    # Calculate differences
    difference_df = calculate_differences(
        clim_df=clim,
        chelsa_df=chelsa_results_df,
        variable_id=variable_id,
        method=mm  # Change to 'ratio' if needed
    )

    # Apply differences
    final_df = apply_differences(
        hist_df=hist,
        cmip_df=cmip,
        difference_df=difference_df,
        variable_id=variable_id,
        method=method  # Change to 'multiply' if needed
    )

    return final_df





import numpy as np
import pandas as pd
import xarray as xr
import gcsfs
import fsspec
from multiprocessing import Pool
from dask.diagnostics import ProgressBar
from tqdm import tqdm
import chelsa_cmip6

coords = '/home/karger/scratch/Coordinates_selected_forest_reserves.csv'
variable_id = "pr"
table_id = 'Amon'
experiment_id = 'ssp585'
institution_id = 'MPI-M'
source_id = 'MPI-ESM1-2-LR'
member_id = 'r1i1p1f1'
output_csv_hist = f'/home/karger/{institution_id}_{source_id}_{member_id}_historical_{variable_id}.csv'
output_csv_cmip = f'/home/karger/{institution_id}_{source_id}_{member_id}_{experiment_id}_{variable_id}.csv'
outdir = '/home/karger'


final_df = delta_change_coords(
    coords=coords,
    variable_id=variable_id,
    table_id=table_id,
    experiment_id=experiment_id,
    institution_id=institution_id,
    source_id=source_id,
    member_id=member_id,
    output_csv_hist=output_csv_hist,
    output_csv_cmip=output_csv_cmip,
    n_workers=10,
    outdir=outdir,
    method="multiply"
)

output_csv = f'/home/karger/chelsa_{institution_id}_{source_id}_{member_id}_{experiment_id}_{variable_id}.csv'
final_df.to_csv(output_csv, index=False)








coords = '/home/karger/scratch/Coordinates_selected_forest_reserves.csv'
variable_id = "tas"
table_id = 'Amon'
experiment_id = 'ssp585'
institution_id = 'MPI-M'
source_id = 'MPI-ESM1-2-LR'
member_id = 'r1i1p1f1'
output_csv_hist = f'/home/karger/{institution_id}_{source_id}_{member_id}_historical_{variable_id}.csv'
output_csv_cmip = f'/home/karger/{institution_id}_{source_id}_{member_id}_{experiment_id}_{variable_id}.csv'
outdir = '/home/karger'


final_df = delta_change_coords(
    coords=coords,
    variable_id=variable_id,
    table_id=table_id,
    experiment_id=experiment_id,
    institution_id=institution_id,
    source_id=source_id,
    member_id=member_id,
    output_csv_hist=output_csv_hist,
    output_csv_cmip=output_csv_cmip,
    n_workers=10,
    outdir=outdir,
    method="add"
)

output_csv = f'/home/karger/chelsa_{institution_id}_{source_id}_{member_id}_{experiment_id}_{variable_id}.csv'
final_df.to_csv(output_csv, index=False)





















chelsa_results_df = extract_chelsa(
     variable_id=variable_id,
     csv_file=coords,
     n_workers=10)

hist, clim = extract_pixel_values(
                activity_id='CMIP',
                table_id='Amon',
                variable_id=variable_id,
                experiment_id='historical',
                institution_id=institution_id,
                source_id=source_id,
                member_id=member_id,
                csv_file=coords,
                output_csv='/home/karger/' + institution_id + '_' + source_id + '_' + member_id + '_' + experiment_id + '_' + variable_id + '.csv',
                n_workers=10
            )

cmip = extract_pixel_values(
                activity_id='ScenarioMIP',
                table_id='Amon',
                variable_id=variable_id,
                experiment_id=experiment_id,
                institution_id=institution_id,
                source_id=source_id,
                member_id=member_id,
                csv_file=coords,
                output_csv='/home/karger/' + institution_id + '_' + source_id + '_' + member_id + '_' + experiment_id + '_' + variable_id + '.csv',
                n_workers=10
            )

difference_df = calculate_differences(
    clim_df=clim,
    chelsa_df=chelsa_results_df,
    variable_id='tas',  # Example variable_id
    method='difference'  # Change to 'ratio' if needed
)

final_df = apply_differences(
    hist_df=hist,
    cmip_df=cmip,
    difference_df=difference_df,
    variable_id='tas',  # Example variable_id
    method='add'  # Change to 'multiply' if needed
)

