import numpy as np
import scipy.stats as sps
import warnings

def fit(spsdotwhat, x, fwords):
    """
    Attempts to fit a distribution from the family defined through spsdotwhat
    to the data represented by x, holding parameters fixed according to fwords.

    A maximum likelihood estimation of distribution parameter values is tried
    first. If that fails the method of moments is tried for some distributions.

    Parameters
    ----------
    spsdotwhat : sps distribution class
        Known classes are [sps.norm, sps.weibull_min, sps.gamma, sps.rice,
        sps.beta].
    x : array
        Data to be fitted.
    fwords : dict of str : float
        Keys : 'floc' and (optinally) 'fscale'
        Values : location and (optinally) scale parmeter values that are to be
        held fixed when fitting.

    Returns
    -------
    shape_loc_scale : tuple
        Fitted shape, location, and scale parameter values if fitting worked,
        otherwise None.

    """
    # make sure that there are at least two distinct data points because
    # otherwise it is impossible to fit more than 1 parameter
    if np.unique(x).size < 2:
        msg = 'found fewer then 2 different values in x: returning None'
        warnings.warn(msg)
        return None

    # try maximum likelihood estimation
    try:
        shape_loc_scale = spsdotwhat.fit(x, **fwords)
    except:
        shape_loc_scale = (np.nan,)


    # try method of moment estimation
    if check_shape_loc_scale(spsdotwhat, shape_loc_scale):
        msg = 'maximum likelihood estimation'
        if spsdotwhat == sps.gamma:
            msg += ' failed: method of moments'
            x_mean = np.mean(x) - fwords['floc']
            x_var = np.var(x)
            scale = x_var / x_mean
            shape = x_mean / scale
            shape_loc_scale = (shape, fwords['floc'], scale)
        elif spsdotwhat == sps.beta:
            msg += ' failed: method of moments'
            y = (x - fwords['floc']) / fwords['fscale']
            y_mean = np.mean(y)
            y_var = np.var(y)
            p = np.square(y_mean) * (1. - y_mean) / y_var - y_mean
            q = p * (1. - y_mean) / y_mean
            shape_loc_scale = (p, q, fwords['floc'], fwords['fscale'])
    else:
        msg = ''

    # return result and utter warning if necessary
    if check_shape_loc_scale(spsdotwhat, shape_loc_scale):
        msg += ' failed: returning None'
        warnings.warn(msg)
        return None
    elif msg != '':
        msg += ' succeeded'

    # do rough goodness of fit test to filter out worst fits using KS test
    ks_stat = sps.kstest(x, spsdotwhat.name, args=shape_loc_scale)[0]
    if ks_stat > .5:
        if msg == '': msg = 'maximum likelihood estimation succeeded'
        msg += ' but fit is not good: returning None'
        warnings.warn(msg)
        return None
    else:
        if msg != '':
            warnings.warn(msg)
        return shape_loc_scale





def map_quantiles_non_parametric_brute_force(x, y):
    """
    Quantile-map x to y using the empirical CDFs of x and y.

    Parameters
    ----------
    x : array
        Simulated time series.
    y : array
        Observed time series.

    Returns
    -------
    z : array
        Result of quantile mapping.

    """
    if x.size == 0:
        msg = 'found no values in x: returning x'
        warnings.warn(msg)
        return x

    if np.unique(y).size < 2:
        msg = 'found fewer then 2 different values in y: returning x'
        warnings.warn(msg)
        return x

    p_x = (sps.rankdata(x) - 1.) / x.size  # percent points of x
    p_y = np.linspace(0., 1., y.size)  # percent points of sorted y
    z = np.interp(p_x, p_y, np.sort(y))  # quantile mapping
    return z


def ccs_transfer_sim2obs(
        x_obs_hist, x_sim_hist, x_sim_fut,
        lower_bound=0., upper_bound=1.):
    """
    Generates pseudo future observation(s) by transfering a simulated climate
    change signal to historical observation(s) respecting the given bounds.

    Parameters
    ----------
    x_obs_hist : float or array
        Historical observation(s).
    x_sim_hist : float or array
        Historical simulation(s).
    x_sim_fut : float or array
        Future simulation(s).
    lower_bound : float, optional
        Lower bound of values in input and output data.
    upper_bound : float, optional
        Upper bound of values in input and output data.

    Returns
    -------
    x_obs_fut : float or array
        Pseudo future observation(s).

    """
    # change scalar inputs to arrays
    if np.isscalar(x_obs_hist): x_obs_hist = np.array([x_obs_hist])
    if np.isscalar(x_sim_hist): x_sim_hist = np.array([x_sim_hist])
    if np.isscalar(x_sim_fut): x_sim_fut = np.array([x_sim_fut])

    # check input
    assert lower_bound < upper_bound, 'lower_bound >= upper_bound'
    for x_name, x in zip(['x_obs_hist', 'x_sim_hist', 'x_sim_fut'],
                         [x_obs_hist, x_sim_hist, x_sim_fut]):
        assert np.all(x >= lower_bound), 'found ' + x_name + ' < lower_bound'
        assert np.all(x <= upper_bound), 'found ' + x_name + ' > upper_bound'

    # compute x_obs_fut
    i_neg_bias = x_sim_hist < x_obs_hist
    i_zero_bias = x_sim_hist == x_obs_hist
    i_pos_bias = x_sim_hist > x_obs_hist
    i_additive = np.logical_or(
        np.logical_and(i_neg_bias, x_sim_fut < x_sim_hist),
        np.logical_and(i_pos_bias, x_sim_fut > x_sim_hist))
    x_obs_fut = np.empty_like(x_obs_hist)
    x_obs_fut[i_neg_bias] = upper_bound - \
                            (upper_bound - x_obs_hist[i_neg_bias]) * \
                            (upper_bound - x_sim_fut[i_neg_bias]) / \
                            (upper_bound - x_sim_hist[i_neg_bias])
    x_obs_fut[i_zero_bias] = x_sim_fut[i_zero_bias]
    x_obs_fut[i_pos_bias] = lower_bound + \
                            (x_obs_hist[i_pos_bias] - lower_bound) * \
                            (x_sim_fut[i_pos_bias] - lower_bound) / \
                            (x_sim_hist[i_pos_bias] - lower_bound)
    x_obs_fut[i_additive] = x_obs_hist[i_additive] + \
                            x_sim_fut[i_additive] - x_sim_hist[i_additive]

    # make sure x_obs_fut is within bounds
    x_obs_fut = np.maximum(lower_bound, np.minimum(upper_bound, x_obs_fut))

    return x_obs_fut[0] if x_obs_fut.size == 1 else x_obs_fut


def transfer_odds_ratio(p_obs_hist, p_sim_hist, p_sim_fut):
    """
    Transfers simulated changes in event likelihood to historical observations
    by multiplying the historical odds by simulated future-over-historical odds
    ratio. The method is inspired by the return interval scaling proposed by
    Switanek et al. (2017) <https://doi.org/10.5194/hess-21-2649-2017>.

    Parameters
    ----------
    p_obs_hist : array
        Culmulative probabbilities of historical observations.
    p_sim_hist : array
        Culmulative probabbilities of historical simulations.
    p_sim_fut : array
        Culmulative probabbilities of future simulations.

    Returns
    -------
    p_obs_fut : array
        Culmulative probabbilities of pseudo future observations.

    """
    x = np.sort(p_obs_hist)
    y = np.sort(p_sim_hist)
    z = np.sort(p_sim_fut)

    # interpolate x and y if necessary
    if x.size != z.size or y.size != z.size:
        p_x = np.linspace(0, 1, x.size)
        p_y = np.linspace(0, 1, y.size)
        p_z = np.linspace(0, 1, z.size)
        ppf_x = spi.interp1d(p_x, x)
        ppf_y = spi.interp1d(p_y, y)
        x = ppf_x(p_z)
        y = ppf_y(p_z)

    # transfer
    A = x * (1. - y) * z
    B = (1. - x) * y * (1. - z)
    z_scaled = 1. / (1. + B / A)

    # avoid the generation of unrealistically extreme p-values
    z_min = 1. / (1. + np.power(10., 1. - np.log10(x / (1. - x))))
    z_max = 1. / (1. + np.power(10., -1. - np.log10(x / (1. - x))))
    z_scaled = np.maximum(z_min, np.minimum(z_max, z_scaled))

    return z_scaled[np.argsort(np.argsort(p_sim_fut))]



def percentile1d(a, p):
    """
    Fast version of np.percentile with linear interpolation for 1d arrays
    inspired by
    <https://krstn.eu/np.nanpercentile()-there-has-to-be-a-faster-way/>.

    Parameters
    ----------
    a : array
        Input array.
    p : array
        Percentages expressed as real numbers in [0, 1] for which percentiles
        are computed.

    Returns
    -------
    percentiles : array
        Percentiles

    """
    n = a.size - 1
    b = np.sort(a)
    i = n * p
    i_below = np.floor(i).astype(int)
    w_above = i - i_below
    return b[i_below] * (1. - w_above) + b[i_below + (i_below < n)] * w_above



def map_quantiles_non_parametric_trend_preserving(
        x_obs_hist, x_sim_hist, x_sim_fut,
        trend_preservation='additive', n_quantiles=50,
        max_change_factor=100., max_adjustment_factor=9.,
        adjust_obs=False, lower_bound=None, upper_bound=None):
    """
    Adjusts biases with a modified version of the quantile delta mapping by
    Cannon (2015) <https://doi.org/10.1175/JCLI-D-14-00754.1> or uses this
    method to transfer a simulated climate change signal to observations.

    Parameters
    ----------
    x_obs_hist : array
        Time series of observed climate data representing the historical or
        training time period.
    x_sim_hist : array
        Time series of simulated climate data representing the historical or
        training time period.
    x_sim_fut : array
        Time series of simulated climate data representing the future or
        application time period.
    trend_preservation : str, optional
        Kind of trend preservation:
        'additive'       # Preserve additive trend.
        'multiplicative' # Preserve multiplicative trend, ensuring
                         # 1/max_change_factor <= change factor
                         #                     <= max_change_factor.
        'mixed'          # Preserve multiplicative or additive trend or mix of
                         # both depending on sign and magnitude of bias. Purely
                         # additive trends are preserved if adjustment factors
                         # of a multiplicative adjustment would be greater then
                         # max_adjustment_factor.
        'bounded'        # Preserve trend of bounded variable. Requires
                         # specification of lower_bound and upper_bound. It is
                         # ensured that the resulting values stay within these
                         # bounds.
    n_quantiles : int, optional
        Number of quantile-quantile pairs used for non-parametric quantile
        mapping.
    max_change_factor : float, optional
        Maximum change factor applied in non-parametric quantile mapping with
        multiplicative or mixed trend preservation.
    max_adjustment_factor : float, optional
        Maximum adjustment factor applied in non-parametric quantile mapping
        with mixed trend preservation.
    adjust_obs : boolean, optional
        If True then transfer simulated climate change signal to x_obs_hist,
        otherwise apply non-parametric quantile mapping to x_sim_fut.
    lower_bound : float, optional
        Lower bound of values in x_obs_hist, x_sim_hist, and x_sim_fut. Used
        for bounded trend preservation.
    upper_bound : float, optional
        Upper bound of values in x_obs_hist, x_sim_hist, and x_sim_fut. Used
        for bounded trend preservation.

    Returns
    -------
    y : array
        Result of quantile mapping or climate change signal transfer.

    """
    # make sure there are enough input data for quantile delta mapping
    # reduce n_quantiles if necessary
    assert n_quantiles > 0, 'n_quantiles <= 0'
    n = min([n_quantiles + 1, x_obs_hist.size, x_sim_hist.size, x_sim_fut.size])
    if n < 2:
        if adjust_obs:
            msg = 'not enough input data: returning x_obs_hist'
            y = x_obs_hist
        else:
            msg = 'not enough input data: returning x_sim_fut'
            y = x_sim_fut
        warnings.warn(msg)
        return y
    elif n < n_quantiles + 1:
        msg = 'due to little input data: reducing n_quantiles to %i'%(n-1)
        warnings.warn(msg)
    p_zeroone = np.linspace(0., 1., n)

    # compute quantiles of input data
    q_obs_hist = percentile1d(x_obs_hist, p_zeroone)
    q_sim_hist = percentile1d(x_sim_hist, p_zeroone)
    q_sim_fut = percentile1d(x_sim_fut, p_zeroone)

    # compute quantiles needed for quantile delta mapping
    if adjust_obs: p = np.interp(x_obs_hist, q_obs_hist, p_zeroone)
    else: p = np.interp(x_sim_fut, q_sim_fut, p_zeroone)
    F_sim_fut_inv  = np.interp(p, p_zeroone, q_sim_fut)
    F_sim_hist_inv = np.interp(p, p_zeroone, q_sim_hist)
    F_obs_hist_inv = np.interp(p, p_zeroone, q_obs_hist)

    # do augmented quantile delta mapping
    if trend_preservation == 'bounded':
        msg = 'lower_bound or upper_bound not specified'
        assert lower_bound is not None and upper_bound is not None, msg
        assert lower_bound < upper_bound, 'lower_bound >= upper_bound'
        y = ccs_transfer_sim2obs(
            F_obs_hist_inv, F_sim_hist_inv, F_sim_fut_inv,
            lower_bound, upper_bound)
    elif trend_preservation in ['mixed', 'multiplicative']:
        assert max_change_factor > 1, 'max_change_factor <= 1'
        with np.errstate(divide='ignore', invalid='ignore'):
            y = np.where(F_sim_hist_inv == 0, 1., F_sim_fut_inv/F_sim_hist_inv)
            y[y > max_change_factor] = max_change_factor
            y[y < 1. / max_change_factor] = 1. / max_change_factor
        y *= F_obs_hist_inv
        if trend_preservation == 'mixed':  # if not then we are done here
            assert max_adjustment_factor > 1, 'max_adjustment_factor <= 1'
            y_additive = F_obs_hist_inv + F_sim_fut_inv - F_sim_hist_inv
            fraction_multiplicative = np.zeros_like(y)
            fraction_multiplicative[F_sim_hist_inv >= F_obs_hist_inv] = 1.
            i_transition = np.logical_and(F_sim_hist_inv < F_obs_hist_inv,
                F_obs_hist_inv < max_adjustment_factor * F_sim_hist_inv)
            fraction_multiplicative[i_transition] = .5 * (1. +
                np.cos((F_obs_hist_inv[i_transition] /
                F_sim_hist_inv[i_transition] - 1.) *
                np.pi / (max_adjustment_factor - 1.)))
            y = fraction_multiplicative * y + (1. -
                fraction_multiplicative) * y_additive
    elif trend_preservation == 'additive':
        y = F_obs_hist_inv + F_sim_fut_inv - F_sim_hist_inv
    else:
        msg = 'trend_preservation = '+trend_preservation+' not supported'
        raise AssertionError(msg)

    return y








def map_quantiles_parametric_trend_preserving(
        observed, historical, future,
        variable_type="tas", distribution=None, trend_preservation='additive',
        adjust_p_values=False,
        lower_bound=None, lower_threshold=None,
        upper_bound=None, upper_threshold=None,
        unconditional_ccs_transfer=False, trendless_bound_frequency=False,
        n_quantiles=50, p_value_eps=1e-10,
        max_change_factor=100., max_adjustment_factor=9.):
    """
    Adjusts biases using the trend-preserving parametric quantile mapping method
    described in Lange (2019) <https://doi.org/10.5194/gmd-12-3055-2019>.

    Parameters
    ----------
    observed : array
        Time series of observed climate data representing the historical or
        training time period.
    historical : array
        Time series of simulated climate data representing the historical or
        training time period.
    future : array
        Time series of simulated climate data representing the future or
        application time period.
    variable_type : str
        Type of variable, e.g., "tas" for temperature.
    distribution : str, optional
        Kind of distribution used for parametric quantile mapping:
        [None, 'normal', 'weibull', 'gamma', 'beta', 'rice'].
    trend_preservation : str, optional
        Kind of trend preservation used for non-parametric quantile mapping:
        ['additive', 'multiplicative', 'mixed', 'bounded'].
    adjust_p_values : boolean, optional
        Adjust p-values for a perfect match in the reference period.
    lower_bound : float, optional
        Lower bound of values in observed, historical, and future.
    lower_threshold : float, optional
        Lower threshold of values in observed, historical, and future.
        All values below this threshold are replaced by lower_bound in the end.
    upper_bound : float, optional
        Upper bound of values in observed, historical, and future.
    upper_threshold : float, optional
        Upper threshold of values in observed, historical, and future.
        All values above this threshold are replaced by upper_bound in the end.
    unconditional_ccs_transfer : boolean, optional
        Transfer climate change signal using all values, not only those within
        thresholds.
    trendless_bound_frequency : boolean, optional
        Do not allow for trends in relative frequencies of values below lower
        threshold and above upper threshold.
    n_quantiles : int, optional
        Number of quantile-quantile pairs used for non-parametric quantile
        mapping.
    p_value_eps : float, optional
        In order to keep p-values with numerically stable limits, they are
        capped at p_value_eps (lower bound) and 1 - p_value_eps (upper bound).
    max_change_factor : float, optional
        Maximum change factor applied in non-parametric quantile mapping with
        multiplicative or mixed trend preservation.
    max_adjustment_factor : float, optional
        Maximum adjustment factor applied in non-parametric quantile mapping
        with mixed trend preservation.

    Returns
    -------
    future_adjusted : array
        Result of bias adjustment.

    """
    lower = lower_bound is not None and lower_threshold is not None
    upper = upper_bound is not None and upper_threshold is not None

    # use augmented quantile delta mapping to transfer the simulated
    # climate change signal to the historical observation
    i_obs_hist = np.ones(observed.shape, dtype=bool)
    i_sim_hist = np.ones(historical.shape, dtype=bool)
    i_sim_fut = np.ones(future.shape, dtype=bool)
    if lower:
        i_obs_hist = np.logical_and(i_obs_hist, observed > lower_threshold)
        i_sim_hist = np.logical_and(i_sim_hist, historical > lower_threshold)
        i_sim_fut = np.logical_and(i_sim_fut, future > lower_threshold)
    if upper:
        i_obs_hist = np.logical_and(i_obs_hist, observed < upper_threshold)
        i_sim_hist = np.logical_and(i_sim_hist, historical < upper_threshold)
        i_sim_fut = np.logical_and(i_sim_fut, future < upper_threshold)
    if unconditional_ccs_transfer:
        # use all values
        x_target = map_quantiles_non_parametric_trend_preserving(
            observed, historical, future,
            trend_preservation, n_quantiles,
            max_change_factor, max_adjustment_factor,
            True, lower_bound, upper_bound)
    else:
        # use only values within thresholds
        x_target = observed.copy()
        x_target[i_obs_hist] = map_quantiles_non_parametric_trend_preserving(
            observed[i_obs_hist], historical[i_sim_hist],
            future[i_sim_fut], trend_preservation, n_quantiles,
            max_change_factor, max_adjustment_factor,
            True, lower_threshold, upper_threshold)

    # determine extreme value probabilities of future obs
    if lower:
        p_lower = lambda x : np.mean(x <= lower_threshold)
        p_lower_target = p_lower(observed) \
            if trendless_bound_frequency else ccs_transfer_sim2obs(
            p_lower(observed), p_lower(historical), p_lower(future))
    if upper:
        p_upper = lambda x : np.mean(x >= upper_threshold)
        p_upper_target = p_upper(observed) \
            if trendless_bound_frequency else ccs_transfer_sim2obs(
            p_upper(observed), p_upper(historical), p_upper(future))
    if lower and upper:
        p_lower_or_upper_target = p_lower_target + p_upper_target
        if p_lower_or_upper_target > 1 + 1e-10:
            msg = 'sum of p_lower_target and p_upper_target exceeds one'
            warnings.warn(msg)
            p_lower_target /= p_lower_or_upper_target
            p_upper_target /= p_lower_or_upper_target

    # do a parametric quantile mapping of the values within thresholds
    x_source = future
    y = x_source.copy()

    # determine indices of values to be mapped
    i_source = np.ones(x_source.shape, dtype=bool)
    i_target = np.ones(x_target.shape, dtype=bool)
    if lower:
        # make sure that lower_threshold_source < x_source
        # because otherwise sps.beta.ppf does not work
        lower_threshold_source = \
            percentile1d(x_source, np.array([p_lower_target]))[0] \
                if p_lower_target > 0 else lower_bound if not upper else \
                lower_bound - 1e-10 * (upper_bound - lower_bound)
        i_lower = x_source <= lower_threshold_source
        i_source = np.logical_and(i_source, np.logical_not(i_lower))
        i_target = np.logical_and(i_target, x_target > lower_threshold)
        y[i_lower] = lower_bound
    if upper:
        # make sure that x_source < upper_threshold_source
        # because otherwise sps.beta.ppf does not work
        upper_threshold_source = \
            percentile1d(x_source, np.array([1.-p_upper_target]))[0] \
                if p_upper_target > 0 else upper_bound if not lower else \
                upper_bound + 1e-10 * (upper_bound - lower_bound)
        i_upper = x_source >= upper_threshold_source
        i_source = np.logical_and(i_source, np.logical_not(i_upper))
        i_target = np.logical_and(i_target, x_target < upper_threshold)
        y[i_upper] = upper_bound

    # map quantiles
    while np.any(i_source):
        # break here if target distributions cannot be determined
        if not np.any(i_target):
            msg = 'unable to do any quantile mapping' \
                  + ': leaving %i value(s) unadjusted'%np.sum(i_source)
            warnings.warn(msg)
            break

        # use the within-threshold values of x_sim_fut for the source
        # distribution fitting
        x_source_fit = x_source[i_sim_fut]
        x_target_fit = x_target[i_target]

        # determine distribution parameters
        spsdotwhat = sps.norm if distribution == 'normal' else \
            sps.weibull_min if distribution == 'weibull' else \
                sps.gamma if distribution == 'gamma' else \
                    sps.beta if distribution == 'beta' else \
                        sps.rice if distribution == 'rice' else \
                            None
        if spsdotwhat is None:
            # prepare non-parametric quantile mapping
            x_source_map = x_source[i_source]
            shape_loc_scale_source = None
            shape_loc_scale_target = None
        else:
            # prepare parametric quantile mapping
            if lower or upper:
                # map the values in x_source to be quantile-mapped such that
                # their empirical distribution matches the empirical
                # distribution of the within-threshold values of x_sim_fut
                x_source_map = map_quantiles_non_parametric_brute_force(
                    x_source[i_source], x_source_fit)
            else:
                x_source_map = x_source

            # fix location and scale parameters for fitting
            floc = lower_threshold if lower else None
            fscale = upper_threshold - lower_threshold \
                if lower and upper else None

            # because sps.rice.fit and sps.weibull_min.fit cannot handle
            # fscale=None
            if distribution in ['rice', 'weibull']:
                fwords = {'floc': floc}
            else:
                fwords = {'floc': floc, 'fscale': fscale}

            # fit distributions to x_source and x_target
            shape_loc_scale_source = fit(spsdotwhat, x_source_fit, fwords)
            shape_loc_scale_target = fit(spsdotwhat, x_target_fit, fwords)

        # do non-parametric quantile mapping if fitting failed
        if shape_loc_scale_source is None or shape_loc_scale_target is None:
            msg = 'unable to do parametric quantile mapping' \
                  + ': doing non-parametric quantile mapping instead'
            if spsdotwhat is not None: warnings.warn(msg)
            p_zeroone = np.linspace(0., 1., n_quantiles + 1)
            q_source_fit = percentile1d(x_source_map, p_zeroone)
            q_target_fit = percentile1d(x_target_fit, p_zeroone)
            y[i_source] = map_quantiles_non_parametric_with_constant_extrapolation(
                x_source_map, q_source_fit, q_target_fit)
            break

        # compute source p-values
        limit_p_values = lambda p : np.maximum(p_value_eps,
                                               np.minimum(1-p_value_eps, p))
        p_source = limit_p_values(spsdotwhat.cdf(
            x_source_map, *shape_loc_scale_source))

        # compute target p-values
        if adjust_p_values:
            x_obs_hist_fit = observed[i_obs_hist]
            x_sim_hist_fit = historical[i_sim_hist]
            shape_loc_scale_obs_hist = fit(spsdotwhat,
                                           x_obs_hist_fit, fwords)
            shape_loc_scale_sim_hist = fit(spsdotwhat,
                                           x_sim_hist_fit, fwords)
            if shape_loc_scale_obs_hist is None \
                    or shape_loc_scale_sim_hist is None:
                msg = 'unable to adjust p-values: leaving them unadjusted'
                warnings.warn(msg)
                p_target = p_source
            else:
                p_obs_hist = limit_p_values(spsdotwhat.cdf(
                    x_obs_hist_fit, *shape_loc_scale_obs_hist))
                p_sim_hist = limit_p_values(spsdotwhat.cdf(
                    x_sim_hist_fit, *shape_loc_scale_sim_hist))
                p_target = limit_p_values(transfer_odds_ratio(
                    p_obs_hist, p_sim_hist, p_source))
        else:
            p_target = p_source

        # map quantiles
        y[i_source] = spsdotwhat.ppf(p_target, *shape_loc_scale_target)
        break

    return y
