import xarray as xr

tas = xr.open_dataset("/storage/karger/W5E5/tas_1981-2010.nc")
tasmin = xr.open_dataset("/storage/karger/W5E5/tasmin_1981-2010.nc")
tasmax = xr.open_dataset("/storage/karger/W5E5/tasmax_1981-2010.nc")
pr = xr.open_dataset("/storage/karger/W5E5/pr_1981-2010.nc")

bioclim = BioClim(tas = tas,
                  tasmax = tasmax,
                  tasmin = tasmin,
                  pr = pr)

bio1 = bioclim.bio1()
bio12 = bioclim.bio12()



# Example usage
# pr, tas, tasmax, tasmin should be xarray.DataArray or xarray.Dataset objects
# Replace 'output.nc' with your desired output file name
save_bioclim_variables_to_netcdf(pr, tas, tasmax, tasmin, '/home/karger/scratch/bioclim_1981-2010.nc')