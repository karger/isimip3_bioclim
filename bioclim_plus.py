
import xarray as xr
import numpy as np

from dask.distributed import Client

# Start a Dask client
client = Client()

from functions import *
from BioClim import *

hurs = xr.load_dataset("/storage/karger/W5E5/hurs_W5E5v1.0_19810101-19901231.nc")
sfcWind = xr.load_dataset("/storage/karger/W5E5/sfcWind_W5E5v2.0_19810101-19901231.nc")
tasmax = xr.load_dataset("/storage/karger/W5E5/tasmax_W5E5v1.0_19810101-19901231.nc")
tasmin = xr.load_dataset("/storage/karger/W5E5/tasmin_W5E5v1.0_19810101-19901231.nc")
tas = xr.load_dataset("/storage/karger/W5E5/tas_W5E5v1.0_19810101-19901231.nc")
rlds = xr.load_dataset("/storage/karger/W5E5/rlds_W5E5v2.0_19810101-19901231.nc")
rsds = xr.load_dataset("/storage/karger/W5E5/rsds_W5E5v1.0_19810101-19901231.nc")
ps = xr.load_dataset("/storage/karger/W5E5/ps_W5E5v2.0_19810101-19901231.nc")
pr = xr.load_dataset("/storage/karger/W5E5/pr_W5E5v1.0_19810101-19901231.nc")

# Example usage (assuming the datasets are already loaded into a single xarray.Dataset)
ds = xr.Dataset({
    'relative_humidity': hurs.hurs,
    'surface_wind_speed': sfcWind.sfcWind,
    'maximum_air_temperature': tasmax.tasmax,
    'minimum_air_temperature': tasmin.tasmin,
    'mean_air_temperature': tas.tas,
    'long_wave_solar_radiation': rlds.rlds,
    'short_wave_solar_radiation': rsds.rsds,
    'air_pressure': ps.ps,
    'precipitation': pr.pr
})

# calculate PET
pet_ds = calculate_pet(ds)
pet_ds.to_netcdf("/home/karger/scratch/pet.nc")
ds = ds.persist()

pet_monthly = pet_ds['potential_evapotranspiration'].groupby('time.month').mean('time')
pr_monthly = ds['precipitation'].groupby('time.month').mean('time')

# Calculate CMI
cmi_ds = calculate_cmi(pr_monthly, pet_monthly)
cmi_ds.to_netcdf("/home/karger/scratch/cmi.nc")

# Calculate SWB with a fixed AWC of 220 mm
swb_ds = calculate_swb(cmi_ds['climate_moisture_index'])
swb_ds.to_netcdf("/home/karger/scratch/swb.nc")

# Calculate VPD
vpd_ds = calculate_vpd(ds['maximum_air_temperature'],
                       ds['minimum_air_temperature'],
                       ds['relative_humidity'])

vpd_ds.to_netcdf("/home/karger/scratch/vpd.nc")

# Calclulate FD
frost_days_ds = calculate_frost_days(ds['minimum_air_temperature'])
frost_days_ds.to_netcdf("/home/karger/scratch/fd.nc")

# statistics
hurs_stats = calculate_statistics(ds, "relative_humidity")
hurs_stats.to_netcdf("/home/karger/scratch/hurs.nc")

sfcWind_stats = calculate_statistics(ds, "surface_wind_speed")
sfcWind_stats.to_netcdf("/home/karger/scratch/sfcWind.nc")

rsds_stats = calculate_statistics(ds, "short_wave_solar_radiation")
rsds_stats.to_netcdf("/home/karger/scratch/rsds.nc")


# calculate historical

input_filenames = {
    'hurs': "/storage/karger/isimip3b_bioclim/clim/hurs_W5E5v2.0_19810101-20101231.nc",
    'sfcWind': "/storage/karger/isimip3b_bioclim/clim/sfcWind_W5E5v2.0_19810101-20101231.nc",
    'tasmax': "/storage/karger/isimip3b_bioclim/clim/tasmax_W5E5v2.0_19810101-20101231.nc",
    'tasmin': "/storage/karger/isimip3b_bioclim/clim/tasmin_W5E5v2.0_19810101-20101231.nc",
    'tas': "/storage/karger/isimip3b_bioclim/clim/tas_W5E5v2.0_19810101-20101231.nc",
    'rlds': "/storage/karger/isimip3b_bioclim/clim/rlds_W5E5v2.0_19810101-20101231.nc",
    'rsds': "/storage/karger/isimip3b_bioclim/clim/rsds_W5E5v2.0_19810101-20101231.nc",
    'ps': "/storage/karger/isimip3b_bioclim/clim/ps_W5E5v2.0_19810101-20101231.nc",
    'pr': "/storage/karger/isimip3b_bioclim/clim/pr_W5E5v2.0_19810101-20101231.nc"
}

output_dir = "/storage/karger/isimip3b_bioclim/1981-2010/"

# bioclim+
process_and_save_data(input_filenames, output_dir)

# 19-bioclims
tas = xr.open_dataset("/storage/karger/isimip3b_bioclim/clim/tas_W5E5v2.0_19810101-20101231.nc").groupby('time.month').mean('time')
tasmin = xr.open_dataset("/storage/karger/isimip3b_bioclim/clim/tasmin_W5E5v2.0_19810101-20101231.nc").groupby('time.month').mean('time')
tasmax = xr.open_dataset("/storage/karger/isimip3b_bioclim/clim/tasmax_W5E5v2.0_19810101-20101231.nc").groupby('time.month').mean('time')
pr = xr.open_dataset("/storage/karger/isimip3b_bioclim/clim/pr_W5E5v2.0_19810101-20101231.nc").groupby('time.month').mean('time')

tas = tas.rename({"month": "time"})
tasmax = tasmax.rename({"month": "time"})
tasmin = tasmin.rename({"month": "time"})
pr = pr.rename({"month": "time"})


tas.to_netcdf(output_dir + 'tas_1981-2010.nc')
tasmax.to_netcdf(output_dir + 'tasmax_1981-2010.nc')
tasmin.to_netcdf(output_dir + 'tasmin_1981-2010.nc')
pr.to_netcdf(output_dir + 'pr_1981-2010.nc')

save_bioclim_variables_to_netcdf(pr, tas, tasmax, tasmin, output_dir + '1981-2010/bioclim_1981-2010.nc')




input = "/storage/karger/isimip3b_bioclim/clim/"

models = ['gfdl-esm4', 'ipsl-cm6a-lr', 'mpi-esm1-2-hr', 'mri-esm2-0', 'ukesm1-0-ll']
scenarios = ['ssp126', 'ssp370', 'ssp585']
timeperiods = ['2041_2070', '2071_2100'] #'2011-2040',
for model in models:
    for scenario in scenarios:
        for timeperiod in timeperiods:
            member = 'r1i1p1f1'
            if model == 'ukesm1-0-ll':
                member = 'r1i1p1f2'

            input_filenames = {
                        'hurs': input + model + "_" + member + "_w5e5_" + scenario + "_hurs_global_daily_" + timeperiod + ".nc",
                        'sfcWind': input + model + "_" + member + "_w5e5_" + scenario + "_sfcwind_global_daily_" + timeperiod + ".nc",
                        'tasmax': input + model + "_" + member + "_w5e5_" + scenario + "_tasmax_global_daily_" + timeperiod + ".nc",
                        'tasmin': input + model + "_" + member + "_w5e5_" + scenario + "_tasmin_global_daily_" + timeperiod + ".nc",
                        'tas': input + model + "_" + member + "_w5e5_" + scenario + "_tas_global_daily_" + timeperiod + ".nc",
                        'rlds': input + model + "_" + member + "_w5e5_" + scenario + "_rlds_global_daily_" + timeperiod + ".nc",
                        'rsds': input + model + "_" + member + "_w5e5_" + scenario + "_rsds_global_daily_" + timeperiod + ".nc",
                        'ps': input + model + "_" + member + "_w5e5_" + scenario + "_ps_global_daily_" + timeperiod + ".nc",
                        'pr': input + model + "_" + member + "_w5e5_" + scenario + "_pr_global_daily_" + timeperiod + ".nc",
                    }
            output_dir = "/storage/karger/isimip3b_bioclim/" + timeperiod + "/" + model + "/" + scenario + "/"
            process_and_save_data(input_filenames, output_dir)

            # 19-bioclims
            tas = xr.open_dataset(input + model + "_" + member + "_w5e5_" + scenario + "_tas_global_daily_" + timeperiod + ".nc").groupby(
                'time.month').mean('time')
            tasmin = xr.open_dataset(
                input + model + "_" + member + "_w5e5_" + scenario + "_tasmin_global_daily_" + timeperiod + ".nc").groupby(
                'time.month').mean('time')
            tasmax = xr.open_dataset(
                input + model + "_" + member + "_w5e5_" + scenario + "_tasmax_global_daily_" + timeperiod + ".nc").groupby(
                'time.month').mean('time')
            pr = xr.open_dataset(input + model + "_" + member + "_w5e5_" + scenario + "_pr_global_daily_" + timeperiod + ".nc").groupby(
                'time.month').mean('time')

            tas = tas.rename({"month": "time"})
            tasmax = tasmax.rename({"month": "time"})
            tasmin = tasmin.rename({"month": "time"})
            pr = pr.rename({"month": "time"})

            tas.to_netcdf(output_dir + 'tas.nc')
            tasmax.to_netcdf(output_dir + 'tasmax.nc')
            tasmin.to_netcdf(output_dir + 'tasmin.nc')
            pr.to_netcdf(output_dir + 'pr.nc')

            #bioclim = BioClim(pr, tas, tasmax, tasmin)
            #save_bioclim_variables_to_netcdf(bioclim, output_dir + 'bioclim.nc')


# 19 bioclims

models = ['gfdl-esm4', 'ipsl-cm6a-lr', 'mpi-esm1-2-hr', 'mri-esm2-0', 'ukesm1-0-ll']
scenarios = ['ssp126', 'ssp370', 'ssp585']
timeperiods = ['2041_2070', '2071_2100'] #'2011-2040',
for model in models:
    for scenario in scenarios:
        for timeperiod in timeperiods:
            output_dir = "/storage/karger/isimip3b_bioclim/" + timeperiod + "/" + model + "/" + scenario + "/"
            tas = xr.open_dataset(output_dir + 'tas.nc')
            tasmax = xr.open_dataset(output_dir + 'tasmax.nc')
            tasmin = xr.open_dataset(output_dir + 'tasmin.nc')
            pr = xr.open_dataset(output_dir + 'pr.nc')

            bioclim = BioClim(pr, tas, tasmax, tasmin)
            save_bioclim_variables_to_netcdf(bioclim, output_dir + 'bioclim.nc')