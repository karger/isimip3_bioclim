import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense
import netCDF4 as nc
import rasterio
from scipy.ndimage import zoom


def load_netcdf_data(file_paths, variables, target_resolution=(1000, 1000)):
    data = []
    for file_path, variable in zip(file_paths, variables):
        dataset = nc.Dataset(file_path)
        var_data = dataset.variables[variable][:]
        if var_data.shape[1:] != target_resolution:
            # Resample to target resolution if necessary
            var_data = resample_data(var_data, target_resolution)
        data.append(var_data)
        dataset.close()
    return np.stack(data, axis=-1)


def load_geotiff_data(file_paths):
    data = []
    for file_path in file_paths:
        with rasterio.open(file_path) as src:
            data.append(src.read(1))
    return np.stack(data, axis=-1)


def resample_data(data, target_resolution):
    original_shape = data.shape[1:]
    zoom_factors = [t / o for t, o in zip(target_resolution, original_shape)]
    resampled_data = zoom(data, (1, *zoom_factors), order=0)  # order=0 for nearest neighbor
    return resampled_data


def build_model(input_shape):
    input_layer = Input(shape=input_shape)

    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(input_layer)
    pool1 = MaxPooling2D((2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    pool2 = MaxPooling2D((2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    pool3 = MaxPooling2D((2, 2))(conv3)

    flat = Flatten()(pool3)
    dense1 = Dense(256, activation='relu')(flat)
    output_layer = Dense(1)(dense1)  # Adjust the output layer as per your target variable

    model = Model(inputs=input_layer, outputs=output_layer)
    model.compile(optimizer='adam', loss='mean_squared_error', metrics=['mae'])

    return model


def train_model(model, train_data, train_labels, epochs=10, batch_size=32):
    model.fit(train_data, train_labels, epochs=epochs, batch_size=batch_size, validation_split=0.2)



# Define the variables and base path structure
netcdf_vars = [ 'pr', 'uas', 'vas']
base_path = '/storage/era5/'

# Generate the list of file paths
netcdf_files = [
    f"{base_path}{var}/day/native/1980/{var}_day_era5_198001.nc"
    for var in netcdf_vars
]

# Example usage:
netcdf_files = ['file1.nc', 'file2.nc']  # Add your netCDF file paths
netcdf_vars = ['var1', 'var2']  # Add your variable names
geotiff_files = ['file1.tif', 'file2.tif']  # Add your GeoTIFF file paths

# Load and preprocess data
input_data = load_netcdf_data(netcdf_files, netcdf_vars, target_resolution=(1000, 1000))
target_data = load_geotiff_data(geotiff_files)

# Assuming target data is reshaped to match the input data dimensions
input_shape = input_data.shape[1:]

# Build and train the model
model = build_model(input_shape)
train_model(model, input_data, target_data)




