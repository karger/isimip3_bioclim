#!/usr/bin/env python3
import os
import xarray as xr
import numpy as np
from BioClim import *

def calculate_pet(ds):
    """
    Calculate potential evapotranspiration (PET) using the Penman-Monteith equation from an xarray dataset.

    Parameters:
    ds (xarray.Dataset): Input dataset containing the necessary variables:
                         - relative_humidity
                         - surface_wind_speed
                         - maximum_air_temperature
                         - minimum_air_temperature
                         - mean_air_temperature
                         - long_wave_solar_radiation
                         - short_wave_solar_radiation
                         - air_pressure

    Returns:
    xarray.Dataset: Dataset containing the calculated PET.
    """
    # Constants
    lambda_ = 2.45  # Latent heat of vaporization, MJ/kg
    cp = 1.013e-3  # Specific heat of air, MJ/kg/°C
    epsilon = 0.622  # Ratio molecular weight of water vapor/dry air
    sigma = 4.903e-9  # Stefan-Boltzmann constant, MJ/m2/K4/day

    def penman_monteith(rh, wind, tmax, tmin, tmean, lw_rad, sw_rad, pressure):
        # Convert temperatures from Kelvin to Celsius
        tmax_c = tmax - 273.15
        tmin_c = tmin - 273.15
        tmean_c = tmean - 273.15

        # Convert pressure from Pascal to kPa
        pressure_kPa = pressure / 1000

        # Saturation vapor pressure (es)
        es_tmax = 0.6108 * np.exp(17.27 * tmax_c / (tmax_c + 237.3))
        es_tmin = 0.6108 * np.exp(17.27 * tmin_c / (tmin_c + 237.3))
        es = (es_tmax + es_tmin) / 2

        # Actual vapor pressure (ea)
        ea = rh / 100 * es

        # Slope of vapor pressure curve (delta)
        delta = 4098 * es / ((tmean_c + 237.3) ** 2)

        # Net radiation (Rn)
        Rn = sw_rad * (1 - 0.23) + lw_rad - sigma * ((tmax_c + 273.16) ** 4 + (tmin_c + 273.16) ** 4) / 2 * (
                    0.34 - 0.14 * np.sqrt(ea)) * (1.35 * sw_rad / (sw_rad + 2) - 0.35)

        # Soil heat flux density (G) is often negligible
        G = 0

        # Psychrometric constant (gamma)
        gamma = cp * pressure_kPa / (epsilon * lambda_)

        # Penman-Monteith equation
        PET = (0.408 * delta * (Rn - G) + gamma * (900 / (tmean_c + 273)) * wind * (es - ea)) / (
                    delta + gamma * (1 + 0.34 * wind))
        return PET

    # Apply the function over the time dimension
    pet = xr.apply_ufunc(
        penman_monteith,
        ds['relative_humidity'], ds['surface_wind_speed'], ds['maximum_air_temperature'], ds['minimum_air_temperature'],
        ds['mean_air_temperature'], ds['long_wave_solar_radiation'], ds['short_wave_solar_radiation'],
        ds['air_pressure'],
        input_core_dims=[['time', 'lat', 'lon'], ['time', 'lat', 'lon'], ['time', 'lat', 'lon'], ['time', 'lat', 'lon'],
                         ['time', 'lat', 'lon'], ['time', 'lat', 'lon'], ['time', 'lat', 'lon'],
                         ['time', 'lat', 'lon']],
        output_core_dims=[['time', 'lat', 'lon']],
        vectorize=True,
        dask='parallelized',
        output_dtypes=[np.float32]
    )

    # Convert the result to a dataset and set the name
    pet_ds = pet.to_dataset(name='potential_evapotranspiration')

    return pet_ds



def calculate_cmi(precipitation, pet):
    """
    Calculate the Climate Moisture Index (CMI) using precipitation and PET datasets.

    Parameters:
    precipitation (xarray.DataArray): Input precipitation data in kg m^-2 s^-1.
    pet (xarray.DataArray): Input potential evapotranspiration data in mm/day.

    Returns:
    xarray.DataArray: DataArray containing the calculated CMI.
    """
    # Convert precipitation from kg m^-2 s^-1 to mm/day
    seconds_per_day = 86400*30  # 60 * 60 * 24
    precipitation_mm_per_day = precipitation * seconds_per_day

    # Calculate CMI as the difference between precipitation and PET
    cmi = precipitation_mm_per_day - pet

    # Convert the result to a dataset and set the name
    cmi_ds = cmi.to_dataset(name='climate_moisture_index')

    return cmi_ds


def calculate_swb(cmi, awc_value=220):
    """
    Calculate the Site Water Balance (SWB) using CMI and a fixed available water capacity (AWC).

    Parameters:
    cmi (xarray.DataArray): Input climate moisture index data with monthly climatology (12 values).
    awc_value (float): Available water capacity of the soil, default is 220 mm.

    Returns:
    xarray.DataArray: DataArray containing the calculated SWB.
    """

    def swb_per_pixel(cmi_pixel):
        """
        Calculate the SWB for a single pixel's climatological year of CMI with fixed AWC.
        """
        # Find the month with the lowest CMI to start the hydrological year
        start_month = np.argmin(cmi_pixel)
        cmi_rolled = np.roll(cmi_pixel, -start_month)  # Shift CMI to start hydrological year
        swb_sum = 0

        for monthly_cmi in cmi_rolled:
            # Update SWB sum
            swb_sum += monthly_cmi

            # Ensure positive values do not exceed AWC
            if swb_sum > 0:
                swb_sum = min(swb_sum, awc_value)

        return swb_sum

    # Apply the function along the month dimension
    swb = xr.apply_ufunc(
        swb_per_pixel,
        cmi,
        input_core_dims=[['month']],
        output_core_dims=[[]],  # Output does not have the 'month' dimension
        vectorize=True,
        dask='allowed',
        output_dtypes=[np.float32]
    )

    # Convert the result to a DataArray and set the name
    swb_da = xr.DataArray(swb, coords={dim: cmi.coords[dim] for dim in cmi.dims if dim != 'month'},
                          name='site_water_balance')

    return swb_da
def calculate_vpd(tasmax, tasmin, hurs):
    """
    Calculate the Vapor Pressure Deficit (VPD) using maximum and minimum air temperature and relative humidity.

    Parameters:
    tasmax (xarray.DataArray): Input maximum air temperature in Kelvin.
    tasmin (xarray.DataArray): Input minimum air temperature in Kelvin.
    hurs (xarray.DataArray): Input relative humidity in percent.

    Returns:
    xarray.DataArray: DataArray containing the calculated VPD.
    """
    # Convert temperatures from Kelvin to Celsius
    tasmax_celsius = tasmax - 273.15
    tasmin_celsius = tasmin - 273.15

    # Calculate saturation vapor pressure for max and min temperatures
    es_tasmax = 0.6108 * np.exp((17.27 * tasmax_celsius) / (tasmax_celsius + 237.3))
    es_tasmin = 0.6108 * np.exp((17.27 * tasmin_celsius) / (tasmin_celsius + 237.3))

    # Calculate mean saturation vapor pressure
    es_mean = (es_tasmax + es_tasmin) / 2

    # Calculate actual vapor pressure
    ea = es_mean * (hurs / 100.0)

    # Calculate VPD
    vpd = es_mean - ea

    # Convert the result to a dataset and set the name
    vpd_ds = vpd.to_dataset(name='vapor_pressure_deficit')

    return vpd_ds


def calculate_frost_days(tasmin):
    """
    Calculate the number of frost days per year.

    Parameters:
    tasmin (xarray.DataArray): Input minimum air temperature data (in Kelvin).

    Returns:
    xarray.DataArray: DataArray containing the calculated number of frost days.
    """
    # Convert temperature from Kelvin to Celsius
    tasmin_celsius = tasmin - 273.15

    # Determine when minimum temperature falls below 0°C
    frost_days = tasmin_celsius < 0

    # Sum frost days over the time dimension to get the total number of frost days per year
    frost_days_sum = frost_days.sum(dim='time')

    # Convert the result to a dataset and set the name
    frost_days_ds = frost_days_sum.to_dataset(name='frost_days')

    return frost_days_ds


def calculate_statistics(ds, variable):
    """
    Calculate the mean, standard deviation, and range over the time axis for a given variable in an xarray Dataset.

    Parameters:
    ds (xarray.Dataset): The input dataset containing various climate variables.
    variable (str): The name of the variable to calculate the statistics for.

    Returns:
    xarray.Dataset: A dataset containing the mean, standard deviation, and range of the specified variable.
    """
    if variable not in ds:
        raise ValueError(f"Variable '{variable}' not found in the dataset")

    # Calculate the mean, standard deviation, and range over the time axis
    mean_val = ds[variable].mean(dim='time')
    std_val = ds[variable].std(dim='time')
    range_val = ds[variable].max(dim='time') - ds[variable].min(dim='time')

    # Create a new dataset to hold the results
    result_ds = xr.Dataset({
        f'{variable}_mean': mean_val,
        f'{variable}_std': std_val,
        f'{variable}_range': range_val
    })

    return result_ds




def process_and_save_data(input_filenames, output_dir):
    """
    Read datasets from input filenames, process them, and save the output files to the output directory.

    Parameters:
    input_filenames (dict): Dictionary containing input filenames for each variable.
    output_dir (str): Directory to save the output files.

    Returns:
    None
    """
    # Load datasets
    hurs = xr.open_dataset(input_filenames['hurs'])['hurs']
    try:
        sfcWind = xr.open_dataset(input_filenames['sfcWind'])['sfcWind']
    except KeyError:
        sfcWind = xr.open_dataset(input_filenames['sfcWind'])['sfcwind']

    tasmax = xr.open_dataset(input_filenames['tasmax'])['tasmax']
    tasmin = xr.open_dataset(input_filenames['tasmin'])['tasmin']
    tas = xr.open_dataset(input_filenames['tas'])['tas']
    rlds = xr.open_dataset(input_filenames['rlds'])['rlds']
    rsds = xr.open_dataset(input_filenames['rsds'])['rsds']
    ps = xr.open_dataset(input_filenames['ps'])['ps']
    pr = xr.open_dataset(input_filenames['pr'])['pr']

    # Create xarray Dataset
    ds = xr.Dataset({
        'relative_humidity': hurs,
        'surface_wind_speed': sfcWind,
        'maximum_air_temperature': tasmax,
        'minimum_air_temperature': tasmin,
        'mean_air_temperature': tas,
        'long_wave_solar_radiation': rlds,
        'short_wave_solar_radiation': rsds,
        'air_pressure': ps,
        'precipitation': pr
    })

    # Calculate PET and save to file
    pet_ds = calculate_pet(ds)
    pet_ds.to_netcdf(os.path.join(output_dir, "pet.nc"))

    # Calculate monthly means for precipitation and PET
    pet_monthly = pet_ds['potential_evapotranspiration'].groupby('time.month').mean('time')
    pr_monthly = ds['precipitation'].groupby('time.month').mean('time')

    # Calculate CMI and save to file
    cmi_ds = calculate_cmi(pr_monthly, pet_monthly)
    cmi_ds.to_netcdf(os.path.join(output_dir, "cmi.nc"))

    # Calculate SWB with a fixed AWC of 220 mm and save to file
    swb_ds = calculate_swb(cmi_ds['climate_moisture_index'])
    swb_ds.to_netcdf(os.path.join(output_dir, "swb.nc"))

    # Calculate VPD and save to file
    vpd_ds = calculate_vpd(ds['maximum_air_temperature'],
                           ds['minimum_air_temperature'],
                           ds['relative_humidity'])
    vpd_ds.to_netcdf(os.path.join(output_dir, "vpd.nc"))

    # Calculate frost days and save to file
    frost_days_ds = calculate_frost_days(ds['minimum_air_temperature'])
    frost_days_ds.to_netcdf(os.path.join(output_dir, "fd.nc"))

    # Calculate statistics for variables and save to files
    for var in ['relative_humidity', 'surface_wind_speed', 'short_wave_solar_radiation']:
        var_stats = calculate_statistics(ds, var)
        var_stats.to_netcdf(os.path.join(output_dir, f"{var}_stats.nc"))

    return True


def save_bioclim_variables_to_netcdf(bioclim, output_file):
    # Instantiate the BioClim class
    # bioclim = BioClim(pr, tas, tasmax, tasmin)

    # List of bio* methods to call
    bio_methods = ['bio1', 'bio2', 'bio3', 'bio4', 'bio5', 'bio6', 'bio7', 'bio8', 'bio9',
                   'bio10', 'bio11', 'bio12', 'bio13', 'bio14', 'bio15', 'bio16', 'bio17',
                   'bio18', 'bio19', 'gdd']

    # Initialize an empty list to hold the results
    bio_datasets = []

    # Loop over the methods, call them, and store the results
    for method in bio_methods:
        bio_func = getattr(bioclim, method)
        bio_dataset = bio_func()
        bio_datasets.append(bio_dataset)

    # Combine all datasets into a single dataset
    combined_dataset = xr.merge(bio_datasets)

    # Save the combined dataset to a NetCDF file
    combined_dataset.to_netcdf(output_file)
    print(f"Bioclimatic variables saved to {output_file}")

    return True


